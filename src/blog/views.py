from __future__ import unicode_literals
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q , Min , Max
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect

from .models import Post, PostTag
from tourism.models import Place, PlaceTag
from users.models import Blogger 
from django.db.models import Q
from common_utils import slack_error


def get_next_id(curr_id):
    try:
        ret = Post.objects.filter(status=1).filter(id__gt=curr_id).order_by("id")[0:1].get().id
    except Post.DoesNotExist:
        ret = Post.objects.aggregate(Min("id"))['id__min']
    return ret

def get_prev_id(curr_id):
    try:
        ret = Post.objects.filter(status=1).filter(id__lt=curr_id).order_by("-id")[0:1].get().id
    except Post.DoesNotExist:
        ret = Post.objects.aggregate(Max("id"))['id__max']
    return ret


def get_parsed_description(post):
    txt = post.description
    des_list = list(map(unicode,txt.split("\n\r")))
    img_list = post.image.all()
    length = max(len(des_list),len(img_list))
    image_description_data = [{'image': None, 'description': None} for _ in range(length)]
    for i in range(length):
        if i<len(des_list):
            image_description_data[i]['description'] = des_list[i]
        if i<len(img_list):
            image_description_data[i]['image'] = img_list[i]
    
    return image_description_data

#------------------------------- Suggestion Utility -----------------------------------

def get_places_by_direct_tags(post):
    tags = post.tag.all()
    places = Place.objects.filter(name__in=[tag.name for tag in tags]).all()
    return places

def max_tags_matched_places(post):
    tags = post.tag.all()
    place_tag = {}
    for tag in tags:
        places = Place.objects.filter(tag=tag).all()
        for place in places:
            if place.id in place_tag:
                place_tag[place.id] += 1
            else:
                place_tag[place.id] = 1
    place_tag_count = set()
    for place_id, tag_count in place_tag.items():
        place_tag_count.add(tag_count)
    place_tag_count_list = list(place_tag_count)
    place_tag_count_list.sort(reverse=True)
    max_count, counter = 3, 0
    place_ids = []
    for tcount in place_tag_count_list:
        for place_id, tag_count in place_tag.items():
            if tag_count == tcount:
                place_ids.append(place_id)
                counter += 1
            if counter == 3:
                return Place.objects.filter(id__in=place_ids).all()
    return Place.objects.filter(id__in=place_ids).all()

def get_place_suggestion(post):
    for criteria in place_suggestion_criteria:
        if post.type.type == criteria['post_type']:
            return criteria['function'](post)

place_suggestion_criteria = [
        {'post_type': 'Travel', 'function' : get_places_by_direct_tags},
        {'post_type': 'Food', 'function': max_tags_matched_places},
        {'post_type': 'Culture', 'function': max_tags_matched_places},
        {'post_type': 'Others', 'function': max_tags_matched_places},
    ]        

#-------------------------------------------------------------------------------------

def post_detail(request, slug=None):
    post = get_object_or_404(Post, slug=slug, status=1)
    image_description_data = get_parsed_description(post)
    try:
        post.view += 1
        post.save()
        next_post = get_object_or_404(Post, id=get_next_id(post.id))
        prev_post = get_object_or_404(Post, id=get_prev_id(post.id))
        place_suggestion = get_place_suggestion(post)
        post_latest = Post.objects.filter(status=1).exclude(id=post.id).order_by('-cr_date')[:4]
        post_author = Post.objects.filter(status=1).filter(blogger=post.blogger).exclude(id=post.id).exclude(id__in=[p.id for p in post_latest]).order_by('-cr_date')[:4]
        context={
                "title": post.title,
                "post": post,
                "image_description_data": image_description_data,
                "next": next_post,
                "prev": prev_post,
                "post_latest": post_latest,
                "post_author": post_author,
                "place_suggestion": place_suggestion,
        }
    except Exception as e:
        slack_error('exception in post detail for %s with error as %s' % (slug,str(e)))
    return render(request, "blog/post_detail.html", context)

def blogger_detail(request, id=None):
    blogger = get_object_or_404(Blogger, id=id, status=1)
    try:
        posts = Post.objects.filter(blogger_id=id, status=1)
        views = 0
        for post in posts:
            views += post.view 
        context={
            "title" : blogger.user.fname +" "+ blogger.user.lname,
            "blogger": blogger,  
            "posts": posts, 
            "views": views, 
        }
    except Exception as e:
        print(e)
        slack_error('exception in blogger detail for %s with error as %s' % (id,str(e)))
       
    return render(request, "blog/blogger_detail.html", context)


def blogger_list(request):
    bloggers = Blogger.objects.filter(status=1).exculde(id=5).all()
    try: 
        context={
            "title" : "Bloggers of Uttarakhand Traveller",
            "bloggers": bloggers,  
        }
    except Exception as e:
        print(e)
        slack_error('exception in blogger list for  with error as %s' % (str(e)))
       
    return render(request, "blog/blogger_list.html", context)


def post_list(request):
    #posts = Post.objects.all()
    posts = Post.objects.filter(status=1)
    try:
        query = request.GET.get("q")
        if query:
            posts = posts.filter(
                Q(title__icontains=query) |
                Q(description__icontains=query)
            ).filter(status=1).distinct()
        paginator = Paginator(posts, 5)  # Show 5 posts per page
        page_request_var = "page"
        page = request.GET.get(page_request_var)
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            posts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            posts = paginator.page(paginator.num_pages)
        no_posts = ''
        if len(posts)==0:
            no_posts = 'No Search Results'

        context = {
            "posts": posts,
            "title": "Articles",
            "page_request_var": page_request_var,
            #"no_posts": no_posts,
        }
    except Exception as e:
        slack_error('exception in post list with error as %s' % str(e))
    return render(request, "blog/post_list.html", context)
