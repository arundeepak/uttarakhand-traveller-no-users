from django.contrib import admin

from .models import Post, PostTag, PhotoBank
# Register your models here.

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    class meta:
	    model = Post

class PhotoBankAdmin(admin.ModelAdmin):
    list_display = ('id', 'url', 'location', 'username', 'cr_date',)
    list_display_links = ('id', 'location', 'username')

admin.site.register(Post,PostAdmin)
admin.site.register(PostTag)
admin.site.register(PhotoBank,PhotoBankAdmin)