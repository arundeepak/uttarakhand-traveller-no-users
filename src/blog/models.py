from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone


from django.db.models.signals import post_save
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.dispatch import receiver

from users.models import Blogger
from tourism.models import Tag,Image

# Create your models here.


class PostManager(models.Manager):
    def active(self, *args, **kwargs):
        # Post.objects.all() = super(PostManager, self).all()
        return super(PostManager, self).filter(draft=False).filter(publish__lte=timezone.now())


def upload_location(instance, filename='places'):
    return "%s/%s/%s"%('blog', instance.id, filename)



class PostType(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=300)


    class Meta:
        managed = False
        db_table = 'post_type'
        verbose_name = 'post_type'
        verbose_name_plural = 'post_types'

    objects = PostManager()

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)



class Post(models.Model):
    id = models.AutoField(primary_key=True)
    blogger = models.ForeignKey(Blogger)
    image_url = models.ImageField(upload_to=upload_location, blank=True)
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    description = models.TextField()
    cr_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    status = models.BooleanField(default=True)
    tag = models.ManyToManyField(Tag)
    image = models.ManyToManyField(Image, blank=True)
    view = models.IntegerField()
    type = models.ForeignKey(PostType)
    class Meta:
        managed = False
        db_table = 'post'
        verbose_name = 'post'
        verbose_name_plural = 'posts'
        ordering = ["-cr_date"]

    def __unicode__(self):
       return self.title
    
    def __str__(self):
       return self.title

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={"slug": self.slug})



class PostTag(models.Model):
    id = models.AutoField(primary_key=True)
    post = models.ForeignKey(Post)
    tag = models.ForeignKey(Tag)

    class Meta:
        managed = False
        db_table = 'post_tag'
        verbose_name = 'post_tag'
        verbose_name_plural = 'post_tags'

    objects = PostManager()

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)


class PhotoBank(models.Model):
    id        = models.AutoField(primary_key=True)
    url       = models.ImageField()
    username  = models.CharField(max_length=300)
    location  = models.CharField(max_length=300)
    cr_date   = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        managed = False
        db_table = 'photo_bank'
        verbose_name = 'photo_bank'
        verbose_name_plural = 'photo_bank'


    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)


class PostImage(models.Model):
    id = models.AutoField(primary_key=True)
    Post = models.ForeignKey(Post)
    image = models.ForeignKey(Image)


    class Meta:
        managed = False
        db_table = 'post_image'
        verbose_name = 'post_image'
        verbose_name_plural = 'post_images'

    def __unicode__(self):
        return str(self.id)

    def __str__(self):
        return str(self.id)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
