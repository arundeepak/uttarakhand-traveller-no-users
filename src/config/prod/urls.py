"""uttarakhandtravellerv1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.contrib import admin
from users.views import home,instagram,terms,facebook,comingsoon,email,success,about
from blog.views import post_list, post_detail,blogger_detail,blogger_list

urlpatterns = [
    url(r'^$',home, name='home'),
    url(r'^posts/$',post_list, name='post_list'),
    url(r'^posts/(?P<slug>[-\w\d]+)$',post_detail,name='post_detail'),
    url(r'^bloggers/$',blogger_list, name='blogger_list'),
    url(r'^bloggers/(?P<id>\d+)$',blogger_detail,name='blogger_detail'),
    #url(r'^contact/$', contact, name='contact'),
    url(r'^contact/$', email, name='contact'),
    url(r'^about/$',about, name='about'),
    url(r'^success/$', success, name='success'),
    #url(r'^instaposts/$', instagram, name='instagram_posts'),
    url(r'^fbposts/$', facebook, name='facebook_posts'),
    url(r'^tos/$', terms, name='terms_and_conditions'),
    url(r'^comingsoon/$', comingsoon, name='comingsoon'),
    #url(r'^posts/', include('blog.urls',namespace='posts')),
    url(r'^places/', include('tourism.urls',namespace='places')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('api.urls')),
]

handler404 = 'users.views.urlnotfound'
handler500 = 'users.views.servererror'

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
