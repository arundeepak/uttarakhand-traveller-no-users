from rest_framework import generics
from .serializers import BlogSerializer,PlaceSerializer
from blog.models import Post
from tourism.models import Place

class CreatePostView(generics.ListCreateAPIView):
	queryset = Post.objects.all()
	serializer_class = BlogSerializer

	def perform_create(self, serializer):
		serializer.save()

class PostDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = BlogSerializer

class PlaceDetailsView(generics.RetrieveUpdateDestroyAPIView):
	serializer_class = PlaceSerializer
	queryset = Place.objects.all()

