from rest_framework import serializers
from blog.models import Post
from tourism.models import Place,District,Tag,Image


class BlogSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = (
                'id', 'blogger','title','image_url','description','status','cr_date',
        )
        read_only_fields = (
            'cr_date',
        )


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ('id', 'name', 'division',)

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id','url','status',)

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'status',)

class CustomDistrictSerializer(serializers.RelatedField):
    def to_representation(self, value):
        print(value)
        return 'district %d: %s (%s)' % (value.id, value.name, value.division)

class PlaceSerializer(serializers.ModelSerializer):
    district = DistrictSerializer(instance=Place,read_only=True)
    tag = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)
    class Meta:
        model = Place
        fields = (
                'id', 'district','name','description','destination','status','cr_date','image','tag',
        )
        read_only_fields = (
            'cr_date',
        )
