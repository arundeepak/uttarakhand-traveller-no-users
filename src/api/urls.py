from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreatePostView,PostDetailsView,PlaceDetailsView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = {
    url(r'^api/create$', CreatePostView.as_view(), name="create_post"),
    url(r'^api/post/(?P<pk>[0-9]+)$',PostDetailsView.as_view(), name="post_details"),
    url(r'^api/place/(?P<pk>[0-9]+)$',PlaceDetailsView.as_view(), name="place_details"),
    url(r'^api/get-token/', obtain_auth_token),
}

urlpatterns = format_suffix_patterns(urlpatterns)
