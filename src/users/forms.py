from django import forms

# our new form
class ContactForm(forms.Form):
    from_email = forms.EmailField(widget=forms.TextInput(attrs={'size':50}),label="Your Email",required=True)
    subject = forms.CharField(widget=forms.TextInput(attrs={'size':50}),required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={'width':"100%", 'cols' : "80", 'rows': "20", }), required=True)
