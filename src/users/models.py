from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.core.urlresolvers import reverse

class User(models.Model):
    id = models.AutoField(primary_key=True)
    fname = models.CharField(max_length=30, null=False)
    lname = models.CharField(max_length=30, null=True)
    email = models.CharField(max_length=30, null=True)
    password_salt = models.CharField(max_length=30, null=False)
    authcode = models.CharField(max_length=30, null=True)
    phone = models.CharField(max_length=15, null=False)
    created_dt = models.DateTimeField(auto_now_add=True, null=False)
    status = models.BooleanField(default=True)

    class Meta:
        managed = False
        db_table = 'user'
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __unicode__(self):
        return self.fname+" "+self.lname
    
    def __str__(self):
        return self.fname+" "+self.lname

class Blogger(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User)
    short_bio = models.CharField(max_length=300, blank=True)
    pass_code = models.IntegerField()
    status = models.BooleanField(default=True)
    posts = models.IntegerField(default=0)
    image = models.CharField(max_length=300)
    description = models.TextField()

    class Meta:
        managed = False
        db_table = 'blogger'
        verbose_name = 'blogger'
        verbose_name_plural = 'bloggers'

    def __unicode__(self):
        return str(self.user.fname +" "+ self.user.lname)

    def __str__(self):
        return str(self.user.fname +" "+ self.user.lname)

    def get_absolute_url(self):
        return reverse("blogger_detail", kwargs={"id": self.id})