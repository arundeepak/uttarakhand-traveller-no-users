from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect
from blog.models import Post
from tourism.models import Spot, Temple, Place
from instagram.client import InstagramAPI
from .facebook_posts import my_facebook
from django.core.mail import send_mail, BadHeaderError
from users.forms import ContactForm
from random import shuffle
from common_utils import slack_error



def home(request):
    try:
        blog_list = [i for i in range(1,Post.objects.all().count()+1)]
        shuffle(blog_list)
        blogs = Post.objects.filter(id__in=blog_list[:3]).all()

        place_list = [i for i in range(1,Place.objects.all().count()+1)]
        place_list.remove(5) # 5 is not there in ids of place
        shuffle(place_list)
        places = Place.objects.filter(id__in=place_list[:3]).all()
        

        spot_list = [i for i in range(1,Spot.objects.all().count()+1)]
        shuffle(spot_list)
        # For testing
        #spot_list = [19,20,21,22]
        spots = Spot.objects.filter(id__in=spot_list[:4]).all()
        
        temple_list = [i for i in range(1,Temple.objects.all().count()+1)]
        shuffle(temple_list)
        # For testing
        #temple_list = [6,13,14,15]
        temples = Temple.objects.filter(id__in=temple_list[:4]).all()

        context={
            'posts' : blogs,
            'places' : places,
            'spots' : spots,
            'temples': temples,
            }
    except Exception as e:
        print(e)
        slack_error('exception in home with error as %s' % str(e))
 

    return render(request, "home.html", context)


def instagram(request):
    try:
        access_token = "5521991788.1677ed0.bc85b80c974348c9b6f93ad16b4bbc42"
        client_secret = "96b500daeaef4a8fad4a2e2a3e420da4"
        api = InstagramAPI(access_token=access_token, client_secret=client_secret)
        medialist = []
        recent_media, next_ = api.user_recent_media(user_id="5521991788")
        for media in recent_media:
            if media.caption:
                print(media.type)
                #medialist.append({'image':media.images['standard_resolution'].url,'text':media.caption.text})
                medialist.append({'image':media.images['standard_resolution'].url,'text':media.caption.text})
        context={
            'instaposts' : medialist,
            'title' : 'Our instagram Posts'
            }
    except Exception as e:
        slack_error('exception in instagran with error as %s' % str(e))

    return render(request, "instagram.html", context)

def facebook(request):
    try:
        fb = my_facebook()
        photos,descs = fb.get_photos()
        #print "photos ",photos
        medialist = []
        for i in range(len(photos)):
            medialist.append({'image':photos[i],'text':descs[i]})
        context={
            'facebookposts' : medialist,
            'title' : 'Facebook Posts'
            }
    except Exception as e:
        slack_error('exception in facebook with error as %s' % str(e))

    return render(request,"facebook.html",context)

def terms(request):
    context = {
        'title' : "Terms and Conditions",
        }
    return render(request,"tos.html",context)


def comingsoon(request):
    context = {
        'title' : 'comingsoon',
    }
    return render(request,"comingsoon.html",context)


def email(request):
    try:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, "sent by "+from_email+"\n"+message, from_email, ['uttarakhandtraveler@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('success')

    except Exception as e:
        slack_error('exception in contact us for email %s with error as %s' % (from_email,str(e)))

    return render(request, "contact.html", {'form': form})


def success(request):
    return HttpResponse('Success! Thank you for your message.')

def about(request):
    return render(request,"about.html",{})

def urlnotfound(request):
    return render(request,"notfound.html",{})

def servererror(request):
    return render(request,"servererror.html",{})
