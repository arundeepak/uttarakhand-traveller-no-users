from django.conf import settings
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.contrib import admin
from .views import district_places, place_detail, place_spots, spot_detail, temple_detail


urlpatterns = [
    url(r'^district/(?P<slug>[-\w\d]+)$',district_places, name='district_places'),
    url(r'^(?P<slug>[-\w\d]+)$', view=place_detail, name='place_detail'),
    url(r'^spots/(?P<slug>[-\w\d]+)$', view=place_spots, name='place_spots'),
    url(r'^spot/(?P<slug>[-\w\d]+)$', view=spot_detail, name='spot_detail'),
    url(r'^temple/(?P<slug>[-\w\d]+)$', view=temple_detail, name='temple_detail'),
    ]

#url(r'^places/', include('tourism.urls',namespace='places')),