import requests
import json
import time
from datetime import date


def get_weather(lat,lon):
    lat, lon = str(lat), str(lon)
    response = requests.get('http://api.openweathermap.org/data/2.5/forecast?lat=%s&lon=%s&appid=e7fe874c90d362a1f5183b2aa6519933' % (lat,lon))
    res = json.loads(response.text)
    weather_days = res.get('list')
    weather_list = []
    first = True
    for day in weather_days:
        e_date = day.get('dt')
        date1 = time.strftime('%Y-%m-%d', time.localtime(e_date))
        if first:
            show_date = time.strftime('%d %b, %Y', time.localtime(e_date))
            weather = day.get('weather')
            weather_list.append({'date': show_date, 'climate': weather[0].get('main')})
            first = False
        today = str(date.today())
        time1 = (time.strftime('%H:%M:%S', time.localtime(e_date)))
        if time1 > '11:30:00' and time1 < '14:30:00' and date1 != today:
            show_date = time.strftime('%d %b, %Y', time.localtime(e_date))
            weather = day.get('weather')
            weather_list.append({'date': show_date, 'climate': weather[0].get('main')})
    return weather_list

#{'Rain', 'Snow', 'Clear', 'Clouds'}
'''
response = requests.get('http://api.openweathermap.org/data/2.5/forecast?lat=30.4886998&lon=79.208266&appid=e7fe874c90d362a1f5183b2aa6519933')
res = json.loads(response.text)
weather_days = res.get('list')
weather_list = []
first = True
for day in weather_days:
    e_date = day.get('dt')
    date1 = time.strftime('%Y-%m-%d', time.localtime(e_date))
    if first:
        show_date = time.strftime('%d %b,%Y', time.localtime(e_date))
        weather = day.get('weather')
        weather_list.append({'date': show_date, 'climate': weather[0].get('main')})
        first = False
    today = str(date.today())
    time1 = (time.strftime('%H:%M:%S', time.localtime(e_date)))
    if '11:30:00' == time1 and date1 != today:
        show_date = time.strftime('%d %b,%Y', time.localtime(e_date))
        weather = day.get('weather')
        weather_list.append({'date': show_date, 'climate': weather[0].get('main')})
#print(weather_list)

for i in weather_list:
    print(i)
'''
