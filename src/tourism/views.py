from random import shuffle
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q , Min , Max
from django.http import Http404
from .models import Place, PlaceImage, Image, Tag, PlaceTag, District, Spot, Temple
from blog.models import Post, PostTag
from .custom import get_weather
from common_utils import slack_error


def get_next_id(curr_id):
    try:
        ret = Place.objects.filter(status=1).filter(id__gt=curr_id).order_by("id")[0:1].get().id
    except Place.DoesNotExist:
        ret = Place.objects.aggregate(Min("id"))['id__min']
    return ret

def get_prev_id(curr_id):
    try:
        ret = Place.objects.filter(status=1).filter(id__lt=curr_id).order_by("-id")[0:1].get().id
    except Place.DoesNotExist:
        ret = Place.objects.aggregate(Max("id"))['id__max']
    return ret

#-------------------------------------------- PLACE ------------------------------------------

def district_places(request,slug=None):
    district = get_object_or_404(District, slug=slug)
    try:
        title =  district.name[0].upper() + district.name[1:]
        places = Place.objects.filter(district=district).filter(status=1).all()
        temples = Temple.objects.filter(district=district).filter(status=1).all()
        if not places:
            raise Http404()
        context={
            "title": title,
            "places": places,
            "temples": temples,
        }
    except Exception as e:
        print(e)
        slack_error('exception in district %s with error as %s' % (slug,str(e)))

    return render(request, "tourism/district_places.html", context)


def place_images(place_id):
    images = Image.objects.filter(placeimage__place_id=place_id).filter(status=1)
    return images


def place_detail(request, slug=None):
    place = get_object_or_404(Place, slug=slug)
    try:
        #place = get_object_or_404(Place, slug=slug)
        id = place.id
        weather_list = get_weather(place.lat,place.lng)
        spots = Spot.objects.filter(place=place).filter(status=1)
        '''weather_list = [{'date': '13 May,2018', 'climate': 'Clear'},
                        {'date': '14 May,2018', 'climate': 'Clear'},
                        {'date': '15 May,2018', 'climate': 'Clouds'},
                        {'date': '16 May,2018', 'climate': 'Rain'},
                        {'date': '17 May,2018', 'climate': 'Snow'}]''' #For testing
        pb_code = place.pb
        district = get_object_or_404(District, id=place.district_id)
        next_place = get_object_or_404(Place, id=get_next_id(id))
        prev_place = get_object_or_404(Place, id=get_prev_id(id))
        images = place_images(place.id)
        hasMul = False
        if len(images)>1:
            hasMul = True
        place_suggestion = Place.objects.filter(district=place.district).exclude(id=place.id).all()[:3]
        post_suggestion = Post.objects.filter(status=1).order_by('-cr_date')[:6]
        spot_counter = 1
        context = {
                "title": place.name,
                "place": place,
                "images": images,
                "district":district,
                "next_place": next_place,
                "prev_place": prev_place,
                "place_suggestion": place_suggestion,
                "post_suggestion": post_suggestion,
                "hasMul" : hasMul,
                "weather_list": weather_list,
                "pb_code": pb_code,
                "spots": spots,
                "spot_counter" : spot_counter,
                "spots_url": "spots/" + place.slug
            }
    except Exception as e:
        slack_error('exception in place %s with error as %s' % (slug,str(e)))

    return render(request, "tourism/place_detail.html", context)


#-------------------------------------- SPOT -------------------------------------------------

def place_spots(request,slug=None):
    place = get_object_or_404(Place, slug=slug)
    try:
        title =  place.name[0].upper() + place.name[1:]
        spots = Spot.objects.filter(place=place).filter(status=1).all()
        places = Place.objects.filter(district=place.district).all()[:3]
        posts = Post.objects.filter(status=1).order_by('-cr_date')[:3]
        if not spots:
            raise Http404()

        context={
            "title": title,
            "spots": spots,
            "places": places,
            "posts": posts,
            "spots_url": "/places/spots/" + place.slug,
        }
    except Exception as e:
        slack_error('exception in place_spots %s with error as %s' % (slug,str(e)))

    return render(request, "tourism/place_spots.html", context)

def spot_detail(request,slug=None):
    spot = get_object_or_404(Spot, slug=slug)
    try:
        title =  spot.name[0].upper() + spot.name[1:]
        images = spot.image.all()
        nearby_spots = Spot.objects.filter(place=spot.place).exclude(id=spot.id).all()
        places = Place.objects.filter(district=spot.place.district).all()[:3]
        posts =Post.objects.filter(status=1).order_by('-cr_date')[:3]
        context={
            "title": title,
            "spot": spot,
            "images": images,
            "spots": nearby_spots,
            "places": places,
            "posts": posts,
            "spots_url": "/places/spots/" + spot.place.slug,
        }
    except Exception as e:
        slack_error('exception in spot_detail %s with error as %s' % (slug,str(e)))

    return render(request, "tourism/spot_detail.html", context)

#-------------------------------------- SPOT -------------------------------------------------


def temple_detail(request,slug=None):
    temple = get_object_or_404(Temple, slug=slug)
    try:
        title =  temple.name[0].upper() + temple.name[1:]
        images = temple.image.all()
        nearby_temples = Temple.objects.filter(district=temple.district).exclude(id=temple.id).all()
        places = Place.objects.filter(district=temple.place.district).all()[:3]
        posts =Post.objects.filter(status=1).order_by('-cr_date')[:3]
        context={
            "title": title,
            "temple": temple,
            "images": images,
            "temples": nearby_temples,
            "places": places,
            "posts": posts,
        }
    except Exception as e:
        slack_error('exception in spot_detail %s with error as %s' % (slug,str(e)))

    return render(request, "tourism/temple_detail.html", context)
