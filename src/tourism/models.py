from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from .choices import DISTRICT_CHOICES,TREK_CHOICES
# Create your models here.


def upload_location(instance, filename='places'):
    return "%s/%s/%s"%('places', instance.id, filename)


class Image(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.ImageField(upload_to=upload_location, null=False, blank=False)
    status = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        super(Image,self).save(*args, **kwargs)

    class Meta:
        managed = False
        db_table = 'image'
        verbose_name = 'image'
        verbose_name_plural = 'images'

    def __unicode__(self):
        return str(self.url)
    
    def __str__(self):
        return str(self.url)



class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    status = models.BooleanField(default=True)

    class Meta:
        managed = False
        db_table = 'tag'
        verbose_name = 'tag'
        verbose_name_plural = 'tags'

    def __unicode__(self):
        return str(self.name)
    
    def __str__(self):
        return str(self.name)


class District(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    slug = models.SlugField(max_length=30)
    division = models.CharField(max_length=10)
    class Meta:
        managed = False
        db_table = 'district'
        verbose_name = 'district'
        verbose_name_plural = 'districts'

    def __unicode__(self):
        return str(self.name)
    
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("places:district_places", kwargs={"slug": self.slug})


class Place(models.Model):
    id = models.AutoField(primary_key=True)
    district = models.ForeignKey(District)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    description = models.TextField()
    destination = models.TextField()
    cr_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    status = models.BooleanField(default=True)
    image = models.ManyToManyField(Image)
    tag = models.ManyToManyField(Tag)
    pb = models.CharField(max_length=500)
    lat = models.DecimalField(decimal_places=3,max_digits=9)
    lng = models.DecimalField(decimal_places=3,max_digits=9)

    class Meta:
        managed = False
        db_table = 'place'
        verbose_name = 'place'
        verbose_name_plural = 'places'
        ordering = ["-cr_date"]

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("places:place_detail", kwargs={"slug": self.slug})


class PlaceImage(models.Model):
    id = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place)
    image = models.ForeignKey(Image)


    class Meta:
        managed = False
        db_table = 'place_image'
        verbose_name = 'place_image'
        verbose_name_plural = 'place_images'

    def __unicode__(self):
        return str(self.id)

    def __str__(self):
        return str(self.id)



class PlaceTag(models.Model):
    id = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place)
    tag = models.ForeignKey(Tag)

    class Meta:
        managed = False
        db_table = 'place_tag'
        verbose_name = 'place_tag'
        verbose_name_plural = 'place_tags'

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)


class Temple(models.Model):
    id = models.AutoField(primary_key=True)
    district = models.ForeignKey(District)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=60)
    deity = models.CharField(max_length=100)
    description = models.TextField()
    cr_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    place = models.ForeignKey(Place)
    status = models.BooleanField(default=True)
    image = models.ManyToManyField(Image, blank=True)
    tag = models.ManyToManyField(Tag)


    class Meta:
        managed = False
        db_table = 'temple'
        verbose_name = 'temple'
        verbose_name_plural = 'temples'
        ordering = ["-cr_date"]

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("places:temple_detail", kwargs={"slug": self.slug})


class TempleImage(models.Model):
    id = models.AutoField(primary_key=True)
    temple = models.ForeignKey(Temple)
    image = models.ForeignKey(Image)


    class Meta:
        managed = False
        db_table = 'temple_image'
        verbose_name = 'temple_image'
        verbose_name_plural = 'temple_images'

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)


class TempleTag(models.Model):
    id = models.AutoField(primary_key=True)
    temple = models.ForeignKey(Temple)
    tag = models.ForeignKey(Tag)

    class Meta:
        managed = False
        db_table = 'temple_tag'
        verbose_name = 'temple_tag'
        verbose_name_plural = 'temple_tags'

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)

class Spot(models.Model):
    id = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    description = models.TextField()
    how_to_reach = models.TextField()
    cr_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    status = models.BooleanField(default=True)
    image = models.ManyToManyField(Image)
    tag = models.ManyToManyField(Tag)

    class Meta:
        managed = False
        db_table = 'spot'
        verbose_name = 'spot'
        verbose_name_plural = 'spots'

    def __unicode__(self):
        return str(self.name)
    
    def __str__(self):
        return str(self.name)
    
    def get_absolute_url(self):
        return reverse("places:spot_detail", kwargs={"slug": self.slug})



class SpotImage(models.Model):
    id = models.AutoField(primary_key=True)
    spot = models.ForeignKey(Spot)
    image = models.ForeignKey(Image)


    class Meta:
        managed = False
        db_table = 'spot_image'
        verbose_name = 'spot_image'
        verbose_name_plural = 'spot_images'

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)


class SpotTag(models.Model):
    id = models.AutoField(primary_key=True)
    spot = models.ForeignKey(Spot)
    tag = models.ForeignKey(Tag)

    class Meta:
        managed = False
        db_table = 'spot_tag'
        verbose_name = 'spot_tag'
        verbose_name_plural = 'spot_tags'

    def __unicode__(self):
        return str(self.id)
    
    def __str__(self):
        return str(self.id)