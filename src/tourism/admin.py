from django.contrib import admin

from .models import Place, PlaceImage, Image, PlaceTag, Tag, Temple, TempleImage, TempleTag, Spot, SpotTag, SpotImage


class PlaceImageAdmin(admin.ModelAdmin):
    list_display = ('place_id','image_id')
    list_display_links = ('place_id','image_id')

class TempleImageAdmin(admin.ModelAdmin):
    list_display = ('temple_id','image_id')
    list_display_links = ('temple_id','image_id')

class PlaceAdmin(admin.ModelAdmin):
    list_display = ('id','district_id','name','cr_date','status','lat','lng')
    list_display_links = ('id','district_id','name','cr_date','status')
    prepopulated_fields = {"slug": ("name",)}
    
class TempleAdmin(admin.ModelAdmin):
    list_display = ('id','district_id','name','deity','place_id','cr_date','status')
    list_display_links = ('id','district_id','name','cr_date','status')
    prepopulated_fields = {"slug": ("name",)}

class ImageAdmin(admin.ModelAdmin):
    list_display = ('id','url','status')
    list_display_links = ('id','url','status')

class TagAdmin(admin.ModelAdmin):
    list_display = ('id','name','status')
    list_display_links = ('id','name','status')

class PlaceTagAdmin(admin.ModelAdmin):
    list_display = ('id','place_id','tag_id')
    list_display_links = ('id','place_id','tag_id')

class TempleTagAdmin(admin.ModelAdmin):
    list_display = ('id','temple_id','tag_id')
    list_display_links = ('id','temple_id','tag_id')


class SpotAdmin(admin.ModelAdmin):
    list_display = ('id','place_id','name','cr_date','status')
    list_display_links = ('id','place_id','name','cr_date','status')
    prepopulated_fields = {"slug": ("name",)}

class SpotTagAdmin(admin.ModelAdmin):
    list_display = ('id','spot_id','tag_id')
    list_display_links = ('id','spot_id','tag_id')

class SpotImageAdmin(admin.ModelAdmin):
    list_display = ('id','spot_id','image_id')
    list_display_links = ('id','spot_id','image_id')

admin.site.register(Place,PlaceAdmin)
admin.site.register(Temple,TempleAdmin)
admin.site.register(Image,ImageAdmin)
admin.site.register(PlaceImage,PlaceImageAdmin)
admin.site.register(TempleImage,TempleImageAdmin)
admin.site.register(Tag,TagAdmin)
admin.site.register(PlaceTag,PlaceTagAdmin)
admin.site.register(TempleTag,TempleTagAdmin)
admin.site.register(Spot,SpotAdmin)
admin.site.register(SpotTag,SpotTagAdmin)
admin.site.register(SpotImage,SpotImageAdmin)