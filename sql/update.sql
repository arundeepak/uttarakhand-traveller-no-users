
/* Branch - newsletter */
create table subscribed_email(
	id INT NOT NULL AUTO_INCREMENT,
    token varchar(60) NOT NULL,
	email varchar(50) NOT NULL,
    cr_date datetime NOT NULL,
    active tinyint default 0 NOT NULL,
    PRIMARY KEY (id)
);



/* Branch - logging */
create table spot(
	id INT NOT NULL AUTO_INCREMENT,
    place_id INT NOT NULL,
	name varchar(50) NOT NULL,
    description text NOT NULL,
    how_to_reach text NOT NULL,
    status tinyint default 0 NOT NULL,
    cr_date datetime NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (place_id) REFERENCES place(id)
);

create table spot_image(
	id INT NOT NULL AUTO_INCREMENT,
    spot_id INT NOT NULL,
    image_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (spot_id) REFERENCES spot(id),
    FOREIGN KEY (image_id) REFERENCES image(id)
);

create table destination_tag(
	id INT NOT NULL AUTO_INCREMENT,
    spot_id INT NOT NULL,
    tag_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (spot_id) REFERENCES spot(id),
    FOREIGN KEY (tag_id) REFERENCES tag(id)
);



create table photo_bank(
    id INT NOT NULL AUTO_INCREMENT,
    url varchar(300) NOT NULL,
    username varchar(300) DEFAULT NULL,
    location varchar(300) NOT NULL,
    cr_date datetime NOT NULL,
    PRIMARY KEY (id)
);

