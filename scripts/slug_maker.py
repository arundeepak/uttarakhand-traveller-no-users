from __future__ import print_function
from builtins import str
import time
from django.template.defaultfilters import slugify

def hit_url():
    import MySQLdb
    db = MySQLdb.connect(host='localhost',    # your host, usually localhost
                         user='uktraveller',         # your username
                         passwd='uttarakhand',  # your password
                         db='uktrav',  # name of the data base
                         )
    cur = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
    #cur.execute("SELECT * FROM PLACE WHERE slug IS NULL")
    #cur.execute("SELECT * FROM DISTRICT WHERE slug IS NULL")
    cur.execute("SELECT * FROM POST WHERE slug IS NULL")
    result = cur.fetchall()
    if result:
        for row in result:
            #url = row['url']
            #title = row['name']
            title = row['title'] #for post
            slug = slugify(title)
            id = row['id']
            #stmt = 'update place set slug="%s" where id=%s' % (slug,id)
            #stmt = 'update district set slug="%s" where id=%s' % (slug,id)
            stmt = 'update post set slug="%s" where id=%s' % (slug,id)
            print(stmt)
            cur.execute(stmt)
    else:
        print('No pending tasks')

    db.commit()
    db.close()

if __name__ == '__main__':
    while True:
        time.sleep(2)
        hit_url()