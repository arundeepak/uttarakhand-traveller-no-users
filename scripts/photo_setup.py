import urllib.request
import time
import MySQLdb
import os 

def fetch_photos():
    db = MySQLdb.connect(   host='localhost',    # your host, usually localhost
                            user='uktraveller',         # your username
                            passwd='uttarakhand',  # your password
                            db='uktrav',  # name of the data base
    )

    cur = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
    cur.execute("SELECT * FROM image where status=1")
    result = cur.fetchall()
    #To Do 
    #photo_store_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'static_in_env')
    #photo_store_path = os.path.join(photo_store_path,'media_root')
    for row in result:
        image = str(row['url'])
        image_name = image.split('/')
        image_name = image_name[-1]
        image_path = "photos/" + image_name
        f = open(image_path,'wb')
        urllib.request.urlretrieve('http://www.uttarakhandtraveller.com/media/'+image,image_path)
        f.close()

def run_one():
    image = '800px-Rani_Jheel_Ranikhet.jpg'
    f = open(image,'wb')
    urllib.request.urlretrieve('http://www.uttarakhandtraveller.com/media/places/None/800px-Rani_Jheel_Ranikhet.jpg',image)
    f.close()

if __name__=='__main__':
    fetch_photos()
    #run_one()
